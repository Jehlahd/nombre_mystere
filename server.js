/* Initialisation du serveur local */

const express = require("express");
const app = express();
const port = 5225;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('./'));

app.get('/', (req, res) => res.sendFile("index.html"));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

/* Tirage d'un nombre alétoire */